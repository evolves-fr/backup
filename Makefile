build:
	goreleaser release --rm-dist --snapshot

lint:
	golangci-lint run

security:
	trivy image registry.gitlab.com/evolves-fr/backup:latest

release:
	goreleaser release --rm-dist

release-chart:
	rm -rf build
	helm package ./charts/backup --destination build
	helm gpg sign -u 4903C52D build/*.tgz
	helm bucket push --bucket charts.evolves.fr --url http://charts.evolves.fr build

coverage:
	@go test -cover -coverprofile coverage.out ./...
	@go tool cover -func coverage.out
