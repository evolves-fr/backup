package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/jinzhu/configor"
	"github.com/urfave/cli/v2"
	"gitlab.com/evolves-fr/backup/core"
	"gitlab.com/evolves-fr/backup/plugins/archive"
	"gitlab.com/evolves-fr/backup/plugins/checksum"
	"gitlab.com/evolves-fr/backup/plugins/firebase/firestore"
	"gitlab.com/evolves-fr/backup/plugins/firebase/storage"
	"gitlab.com/evolves-fr/backup/plugins/folder"
	"gitlab.com/evolves-fr/backup/plugins/mysql"
	"gitlab.com/evolves-fr/backup/plugins/s3"
)

var (
	l core.Core //nolint:gochecknoglobals

	version = "dev"
	commit  = "none"    //nolint:gochecknoglobals
	date    = "unknown" //nolint:gochecknoglobals
)

func main() {
	app := &cli.App{ //nolint:exhaustivestruct
		Name:  "backup",
		Usage: "Manage your input and output backup",
		Description: `Backup is an open-source backup tools written in Go (Golang), with support Amazon S3, MySQL, 
files and folders.`,
		Version: fmt.Sprintf("%s (%s - %s)", version, commit, date),
		Authors: []*cli.Author{
			{Name: "Evolves - Thomas FOURNIER", Email: "tfournier@evolves.fr"},
		},
		Flags: []cli.Flag{
			&cli.StringFlag{ //nolint:exhaustivestruct
				Name:    "config",
				Aliases: []string{"c"},
				EnvVars: []string{"BACKUP_CONFIG"},
				Value:   "/etc/backup/settings.yaml",
				Usage:   "Configuration path `FILE`",
			},
			&cli.StringFlag{ //nolint:exhaustivestruct
				Name:    "log",
				EnvVars: []string{"BACKUP_LOG"},
				Value:   "/dev/stdout",
				Usage:   "Log output path `FILE`",
			},
			&cli.StringFlag{ //nolint:exhaustivestruct
				Name:    "tmp",
				EnvVars: []string{"BACKUP_TMP"},
				Value:   "/tmp/backup/",
				Usage:   "Temp directory path `DIRECTORY`",
			},
			&cli.BoolFlag{ //nolint:exhaustivestruct
				Name:    "debug",
				EnvVars: []string{"BACKUP_DEBUG"},
				Value:   false,
				Usage:   "Enable debug mode",
			},
		},
		Action: action,
	}

	if err := app.Run(os.Args); err != nil {
		l.Logger().Fatal(err)
	}
}

func action(c *cli.Context) error {
	settings := &core.Settings{
		Inputs:       nil,
		Middlewares:  nil,
		Outputs:      nil,
		DisableClean: false,
	}

	if _, err := os.Stat(c.String("config")); os.IsNotExist(err) {
		return errors.Unwrap(err)
	}

	configorConfig := &configor.Config{ErrorOnUnmatchedKeys: true} //nolint:exhaustivestruct

	if err := configor.New(configorConfig).Load(settings, c.String("config")); err != nil {
		return errors.Unwrap(err)
	}

	l = core.New(c.String("tmp"), c.String("log"), c.Bool("debug"))

	l.Register(folder.New())
	l.Register(mysql.New())
	l.Register(s3.New())
	l.Register(checksum.New())
	l.Register(archive.New())
	l.Register(firestore.New())
	l.Register(storage.New())

	l.Run(settings)

	return nil
}
