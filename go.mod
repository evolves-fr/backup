module gitlab.com/evolves-fr/backup

go 1.16

require (
	cloud.google.com/go/storage v1.10.0
	firebase.google.com/go/v4 v4.6.0
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/gabriel-vasile/mimetype v1.3.2-0.20210921080812-220897d54095
	github.com/go-playground/validator/v10 v10.4.1
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/jamf/go-mysqldump v0.6.0
	github.com/jinzhu/configor v1.2.1
	github.com/minio/minio-go/v7 v7.0.10
	github.com/mitchellh/mapstructure v1.4.1
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a // indirect
	golang.org/x/lint v0.0.0-20210508222113-6edffad5e616 // indirect
	golang.org/x/mod v0.5.1 // indirect
	golang.org/x/oauth2 v0.0.0-20210218202405-ba52d332ba99
	golang.org/x/sys v0.0.0-20211004093028-2c5d950f24ef // indirect
	golang.org/x/tools v0.1.7 // indirect
	google.golang.org/api v0.40.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)

replace golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899 => golang.org/x/crypto v0.0.0-20201216223049-8b5274cf687f
