package sum

import (
	"bytes"
	"crypto/sha256"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func New(name string, output string, inputs ...*os.File) (*os.File, error) {
	var buf bytes.Buffer

	for _, file := range inputs {
		var err error

		file, err = os.Open(file.Name())
		if err != nil {
			return nil, errors.Unwrap(err)
		}

		hash := sha256.New()

		if _, err := io.Copy(hash, file); err != nil {
			return nil, errors.Unwrap(err)
		}

		fileName := strings.TrimPrefix(strings.TrimPrefix(file.Name(), output), "/")

		buf.WriteString(fmt.Sprintf("%x\t%s\n", hash.Sum(nil), fileName))

		if err := file.Close(); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	const perm = 0o744

	f, err := os.OpenFile(filepath.Join(output, name), os.O_CREATE|os.O_RDWR, perm)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	if _, err := io.Copy(f, &buf); err != nil {
		return nil, errors.Unwrap(err)
	}

	return f, nil
}
