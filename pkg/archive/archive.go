package archive

import (
	"archive/tar"
	"compress/gzip"
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"
)

func NewTarGz(name string, output string, inputs ...*os.File) (*os.File, error) { //nolint:cyclop
	const perm = 0o744

	f, err := os.OpenFile(filepath.Join(output, name), os.O_CREATE|os.O_RDWR, perm)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	gzipWriter := gzip.NewWriter(f)
	tarWriter := tar.NewWriter(gzipWriter)

	for _, file := range inputs {
		var err error

		file, err = os.Open(file.Name())
		if err != nil {
			return nil, errors.Unwrap(err)
		}

		stat, err := file.Stat()
		if err != nil {
			return nil, errors.Unwrap(err)
		}

		if !stat.IsDir() {
			header, err := tar.FileInfoHeader(stat, file.Name())
			if err != nil {
				return nil, errors.Unwrap(err)
			}

			header.Name = filepath.ToSlash(strings.TrimPrefix(strings.TrimPrefix(file.Name(), filepath.Clean(output)), "/"))

			if err := tarWriter.WriteHeader(header); err != nil {
				return nil, errors.Unwrap(err)
			}

			if _, err := io.Copy(tarWriter, file); err != nil {
				return nil, errors.Unwrap(err)
			}
		}

		if err := file.Close(); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	if err := tarWriter.Close(); err != nil {
		return nil, errors.Unwrap(err)
	}

	if err := gzipWriter.Close(); err != nil {
		return nil, errors.Unwrap(err)
	}

	return f, nil
}
