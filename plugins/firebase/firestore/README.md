# [Plugins:Input] Firebase - Firestore

Plugin for backup FireStore database (FireBase).

## Configuration

| Key           | Type      | Description                                   |          |
|---------------|-----------|-----------------------------------------------|----------|
| name          | string    | Task name                                     | optional |
| credentials   | string    | Google credentials path                       | required |

## Example (`settings.yaml`)

```yaml
input:
  firebase:firestore:
    - credentials: ./my-google-crendials.json
```
