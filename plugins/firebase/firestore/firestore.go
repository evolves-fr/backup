package firestore

import (
	"context"
	"encoding/json"
	"errors"
	"os"
	"path/filepath"
	"strings"

	firebase "firebase.google.com/go/v4"
	"gitlab.com/evolves-fr/backup/core"
	"google.golang.org/api/option"
)

func New() core.InputPlugin {
	return &plugin{}
}

type plugin struct {
	Credentials string `json:"credentials" yaml:"credentials" mapstructure:"credentials" validate:"required"`
}

func (*plugin) Key() string {
	return "firebase:firestore"
}

func (*plugin) Name() string {
	return "Firebase - Cloud Firestore"
}

func (*plugin) Description() string {
	return "Backup Cloud Firestore database"
}

func (i *plugin) Input(ctx *core.Context, cfg interface{}) ([]*os.File, error) {
	if err := ctx.DecodeAndValidate(cfg, i); err != nil {
		return nil, errors.Unwrap(err)
	}

	files := make([]*os.File, 0)

	// Firebase client
	app, err := firebase.NewApp(context.Background(), nil, option.WithCredentialsFile(i.Credentials))
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Firestore client
	client, err := app.Firestore(context.Background())
	if err != nil {
		return nil, errors.Unwrap(err)
	}
	defer client.Close()

	// Get collections
	collections, err := client.Collections(context.Background()).GetAll()
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Enter in collections
	for _, collection := range collections {
		// Get documents
		documents, err := collection.DocumentRefs(context.Background()).GetAll()
		if err != nil {
			return nil, errors.Unwrap(err)
		}

		// Enter in documents
		for _, document := range documents {
			// Get data
			res, err := document.Get(context.Background())
			if err != nil {
				return nil, errors.Unwrap(err)
			}

			// Set outputPath
			outputPath := filepath.Join(ctx.Output, "firebase", res.Ref.Path+".json")
			outputPath = strings.ReplaceAll(outputPath, "(", "")
			outputPath = strings.ReplaceAll(outputPath, ")", "")

			// Create file
			file, err := newFile(outputPath, res.Data())
			if err != nil {
				return nil, err
			}

			files = append(files, file)
		}
	}

	return files, nil
}

func newFile(name string, data interface{}) (*os.File, error) {
	const perm = 0o744

	// Check directory exist
	if _, err := os.Stat(filepath.Dir(name)); os.IsNotExist(err) {
		// Create directory if not exist
		if err := os.MkdirAll(filepath.Dir(name), perm); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	// Convert data to JSON
	dataJSON, err := json.Marshal(data)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Create a new file
	file, err := os.Create(name)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Write JSON data to file
	if _, err := file.Write(dataJSON); err != nil {
		return nil, errors.Unwrap(err)
	}

	// Return file
	return file, nil
}
