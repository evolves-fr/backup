package storage

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"os"
	"path/filepath"
	"strings"
	"time"

	"cloud.google.com/go/storage"
	firebase "firebase.google.com/go/v4"
	"github.com/gabriel-vasile/mimetype"
	"gitlab.com/evolves-fr/backup/core"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/iterator"
	"google.golang.org/api/option"
)

func New() core.InputPlugin {
	return &plugin{}
}

type plugin struct {
	Credentials string  `json:"credentials" yaml:"credentials" mapstructure:"credentials" validate:"required"`
	Bucket      string  `json:"bucket" yaml:"bucket" mapstructure:"bucket" validate:"required"`
	Diff        *string `json:"diff,omitempty" yaml:"diff,omitempty" mapstructure:"diff"`

	projectID   string
	diffRecords map[string]time.Time
}

func (*plugin) Key() string {
	return "firebase:storage"
}

func (*plugin) Name() string {
	return "Firebase - Storage"
}

func (*plugin) Description() string {
	return "Backup Firebase storage"
}

func (i *plugin) Input(ctx *core.Context, cfg interface{}) ([]*os.File, error) {
	if err := ctx.DecodeAndValidate(cfg, i); err != nil {
		return nil, errors.Unwrap(err)
	}

	// Load diff records
	if err := i.LoadDiffRecords(); err != nil {
		return nil, err
	}

	// Load projectID
	if err := i.LoadProjectID(); err != nil {
		return nil, err
	}

	// Firebase client
	app, err := firebase.NewApp(context.Background(), nil, option.WithCredentialsFile(i.Credentials))
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Firebase storage client
	client, err := app.Storage(context.Background())
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Get default bucket
	bucket, err := client.Bucket(i.Bucket)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// List of files
	files := make([]*os.File, 0)

	// Get list of object
	objects := bucket.Objects(context.Background(), &storage.Query{Prefix: ""})

	for {
		attrs, err := objects.Next()
		if errors.Is(err, iterator.Done) {
			break
		}

		if err != nil {
			return nil, errors.Unwrap(err)
		}

		if _, ok := i.diffRecords[attrs.Name]; i.Diff == nil || !ok {
			file, err := i.GetAndWriteObject(ctx, bucket, attrs.Name)
			if err != nil {
				return nil, err
			}

			files = append(files, file)
		}
	}

	// Persist diff records
	if i.Diff != nil {
		if err := i.SaveDiffRecords(); err != nil {
			return nil, err
		}
	}

	return files, nil
}

func (i *plugin) LoadDiffRecords() error {
	if i.Diff == nil {
		return nil
	}

	if _, err := os.Stat(*i.Diff); os.IsNotExist(err) {
		i.diffRecords = make(map[string]time.Time)

		return nil
	}

	bytes, err := os.ReadFile(*i.Diff)
	if err != nil {
		return errors.Unwrap(err)
	}

	return errors.Unwrap(json.Unmarshal(bytes, &i.diffRecords))
}

func (i plugin) SaveDiffRecords() error {
	const perm = 0o744

	data, err := json.MarshalIndent(i.diffRecords, "", "  ")
	if err != nil {
		return errors.Unwrap(err)
	}

	return errors.Unwrap(os.WriteFile(*i.Diff, data, perm))
}

func (i *plugin) LoadProjectID() error {
	credentialsBytes, err := os.ReadFile(i.Credentials)
	if err != nil {
		return errors.Unwrap(err)
	}

	credentials, err := google.CredentialsFromJSON(context.Background(), credentialsBytes)
	if err != nil {
		return errors.Unwrap(err)
	}

	i.projectID = credentials.ProjectID

	return nil
}

func (i plugin) GetAndWriteObject(ctx *core.Context, bucket *storage.BucketHandle, name string) (*os.File, error) {
	// Get object
	obj := bucket.Object(name)

	// Get object attributes
	attrs, err := obj.Attrs(context.Background())
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Set object extension
	if mime := mimetype.Lookup(attrs.ContentType); mime != nil {
		name += mime.Extension()
	}

	// Set output path
	outputPath := filepath.Join(ctx.Output, "firebase", "projects", i.projectID, "storage", i.Bucket, name)

	// Check directory exist
	const perm = 0o744
	if _, err := os.Stat(filepath.Dir(outputPath)); os.IsNotExist(err) {
		// Create directory if not exist
		if err := os.MkdirAll(filepath.Dir(outputPath), perm); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	// Get content reader
	reader, err := obj.NewReader(context.Background())
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Create new file
	file, err := os.Create(outputPath)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	// Write file
	if _, err := io.Copy(file, reader); err != nil {
		return nil, errors.Unwrap(err)
	}

	// If diff enabled, add file
	if i.diffRecords != nil {
		i.diffRecords[strings.TrimSuffix(name, filepath.Ext(name))] = time.Now().UTC()
	}

	return file, nil
}
