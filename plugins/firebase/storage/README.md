# [Plugins:Input] Firebase - Storage

Plugin for backup Firebase storage.

## Configuration

| Key           | Type      | Description                                   |          |
|---------------|-----------|-----------------------------------------------|----------|
| name          | string    | Task name                                     | optional |
| credentials   | string    | Google credentials path                       | required |
| bucket        | string    | Bucket name                                   | required |
| diff          | string    | Diff records file path                        | optional |

## Example (`settings.yaml`)

```yaml
input:
  firebase:storage:
    - credentials: ./my-google-crendials.json
      bucket: my-bucket.appspot.com
      diff: ./records.json
```
