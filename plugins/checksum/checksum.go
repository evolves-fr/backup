package checksum

import (
	"os"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/evolves-fr/backup/core"
	"gitlab.com/evolves-fr/backup/pkg/sum"
)

func New() core.MiddlewarePlugin {
	return &plugin{}
}

type plugin struct {
	Filename string `json:"filename" yaml:"filename" mapstructure:"filename"`
}

func (*plugin) Key() string {
	return "checksum"
}

func (*plugin) Name() string {
	return "Checksum"
}

func (*plugin) Description() string {
	return "Checksum"
}

func (i *plugin) Middleware(ctx *core.Context, cfg interface{}, files []*os.File) ([]*os.File, error) {
	if ctx == nil {
		return nil, core.ContextCantNil
	}

	if err := mapstructure.Decode(cfg, &i); err != nil {
		return nil, err
	}

	if i.Filename == "" {
		i.Filename = "checksum"
	}

	f, err := sum.New(i.Filename+".sha256", ctx.Output, files...)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	files = append(files, f)
	ctx.Log.Infof("Added: %s", f.Name())

	return files, nil
}
