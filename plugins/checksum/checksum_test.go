package checksum

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"

	"gitlab.com/evolves-fr/backup/core"
)

func tmpFile(output string) (*os.File, error) {
	if err := os.MkdirAll(output, 0777); err != nil {
		return nil, err
	}

	file, err := os.OpenFile(filepath.Join(output, "test.txt"), os.O_CREATE|os.O_RDWR, 0777)
	if err != nil {
		return nil, err
	}

	if _, err := file.Write([]byte("hello world")); err != nil {
		return nil, err
	}

	return file, nil
}

func TestPlugin(t *testing.T) {
	p := New()

	assert.Equal(t, "Checksum", p.Name())
	assert.Equal(t, "checksum", p.Key())
	assert.NotEmpty(t, p.Description())
}

func TestMiddlewareCustomValues(t *testing.T) {
	file, err := tmpFile("./tmp/")
	if err != nil {
		t.Error(err)
	}

	ctx := &core.Context{
		Debug:  true,
		Output: "tmp/",
		Log:    logrus.NewEntry(logrus.New()),
	}

	files, err := New().Middleware(ctx, map[interface{}]interface{}{"filename": "files"}, []*os.File{file})
	assert.NoError(t, err)
	assert.Len(t, files, 2)

	res, err := os.ReadFile("./tmp/files.sha256")
	assert.NoError(t, err)
	assert.EqualValues(t, "b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9\ttest.txt\n", string(res))

	os.RemoveAll("./tmp/")
}

func TestMiddlewareDefaultValues(t *testing.T) {
	file, err := tmpFile("./tmp/")
	if err != nil {
		t.Error(err)
	}

	ctx := &core.Context{
		Debug:  true,
		Output: "tmp/",
		Log:    logrus.NewEntry(logrus.New()),
	}

	files, err := New().Middleware(ctx, map[interface{}]interface{}{}, []*os.File{file})
	assert.NoError(t, err)
	assert.Len(t, files, 2)

	res, err := os.ReadFile("./tmp/checksum.sha256")
	assert.NoError(t, err)
	assert.EqualValues(t, "b94d27b9934d3e08a52e52d7da7dabfac484efe37a5380ee9088f7ace2efcde9\ttest.txt\n", string(res))

	os.RemoveAll("./tmp/")
}

func TestMiddlewareError(t *testing.T) {
	ctx := &core.Context{
		Debug:  true,
		Output: "tmp/",
		Log:    logrus.NewEntry(logrus.New()),
	}

	f1, e1 := New().Middleware(nil, nil, nil)
	t.Log(e1)
	assert.Error(t, e1)
	assert.Nil(t, f1)

	f2, e2 := New().Middleware(ctx, "fake", nil)
	t.Log(e2)
	assert.Error(t, e2)
	assert.Nil(t, f2)

	f3, e3 := New().Middleware(ctx, map[interface{}]interface{}{}, nil)
	t.Log(e3)
	assert.Error(t, e3)
	assert.Nil(t, f3)
}
