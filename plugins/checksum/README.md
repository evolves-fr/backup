# [Plugin::Middleware] Checksum

Plugin for checksum files.

## Configuration

| Key       | Type      | Description                                   |          |
|-----------|-----------|-----------------------------------------------|----------|
| name      | string    | Task name                                     | optional |
| filename  | string    | Filename of checksum (without extension)      | optional |

## Example (`settings.yaml`)
### Default
```yaml
middlewares:
  checksum: {}
```

### Custom
```yaml
middlewares:
  checksum:
    filename: files
```
