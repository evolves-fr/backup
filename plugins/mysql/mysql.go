package mysql

import (
	"database/sql"
	"errors"
	"fmt"
	"os"
	"path/filepath"

	validator "github.com/go-playground/validator/v10"

	// Import mysql driver.
	_ "github.com/go-sql-driver/mysql"
	"github.com/jamf/go-mysqldump"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/evolves-fr/backup/core"
)

func New() core.InputPlugin {
	return &plugin{}
}

type plugin struct {
	Host     string `json:"host" yaml:"host" mapstructure:"host" validate:"required"`
	Port     int    `json:"port" yaml:"port" mapstructure:"port" validate:"required"`
	Username string `json:"user" yaml:"user" mapstructure:"user" validate:"required"`
	Password string `json:"pass" yaml:"pass" mapstructure:"pass" validate:"required"`
	Base     string `json:"base" yaml:"base" mapstructure:"base" validate:"required"`
}

func (*plugin) Key() string {
	return "mysql"
}

func (*plugin) Name() string {
	return "MySQL"
}

func (*plugin) Description() string {
	return "Backup MySQL database without mysqldump binary"
}

func (i *plugin) Input(ctx *core.Context, cfg interface{}) ([]*os.File, error) {
	if err := mapstructure.Decode(cfg, &i); err != nil {
		return nil, errors.Unwrap(err)
	}

	if err := validator.New().Struct(i); err != nil {
		return nil, errors.Unwrap(err)
	}

	outputDir := filepath.Join(ctx.Output, "mysql", fmt.Sprintf("%s-%d", i.Host, i.Port))

	if _, err := os.Stat(outputDir); os.IsNotExist(err) {
		const perm = 0o744

		if err := os.MkdirAll(outputDir, perm); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	db, err := sql.Open("mysql", i.DSN())
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	dumper, err := mysqldump.Register(db, outputDir, i.Base)
	if err != nil {
		return nil, errors.Unwrap(err)
	}
	defer dumper.Close()

	if err := dumper.Dump(); err != nil {
		return nil, errors.Unwrap(err)
	}

	if file, ok := dumper.Out.(*os.File); ok {
		return []*os.File{file}, nil
	}

	return nil, nil
}

func (i *plugin) DSN() string {
	return fmt.Sprintf("%s:%s@tcp(%s:%d)/%s",
		i.Username,
		i.Password,
		i.Host,
		i.Port,
		i.Base,
	)
}
