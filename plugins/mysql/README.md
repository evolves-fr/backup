# [Plugins:Input] MySQL

Plugin for backup your MySQL databases without `mysqldump`.

## Configuration

| Key       | Type      | Description                                   |          |
|-----------|-----------|-----------------------------------------------|----------|
| name      | string    | Task name                                     | optional |
| host      | string    | Server address or domain                      | required |
| port      | int       | Server port                                   | required |
| user      | string    | Server username                               | required |
| pass      | string    | Server password                               | required |
| base      | string    | Database name                                 | required |

## Example (`settings.yaml`)
```yaml
input:
  mysql:
    - name: server1
      host: 127.0.0.1
      port: 3306
      user: root
      pass: MyVeryStrongPassword
      base: first-database

    - name: server2
      host: 192.168.0.1
      port: 3306
      user: user
      pass: MyVeryStrongPassword
      base: second-database
```
