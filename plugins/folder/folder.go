package folder

import (
	"errors"
	"io"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/evolves-fr/backup/core"
)

func New() core.InputOutputPlugin {
	return &folder{}
}

type folder struct {
	Path         string `json:"path" yaml:"path" mapstructure:"path" validate:"required"`
	Absolute     bool   `json:"absolute" yaml:"absolute" mapstructure:"absolute"`
	Squash       bool   `json:"squash" yaml:"squash" mapstructure:"squash"`
	RemoveSource bool   `json:"removeSource" yaml:"remove_source" mapstructure:"removeSource"`
}

func (*folder) Key() string {
	return "folder"
}

func (*folder) Name() string {
	return "Folder"
}

func (*folder) Description() string {
	return "Copy file or directory"
}

func (i *folder) Input(ctx *core.Context, cfg interface{}) ([]*os.File, error) {
	if err := mapstructure.Decode(cfg, &i); err != nil {
		ctx.Log.Error(err)

		return nil, nil
	}

	if err := validator.New().Struct(i); err != nil {
		ctx.Log.Error(err)

		return nil, nil
	}

	var files []*os.File

	err := filepath.Walk(i.Path, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		subFolder := "folder"

		if i.Absolute {
			abs, err := filepath.Abs(path)
			if err != nil {
				ctx.Log.Warn(err)
			} else {
				path = abs
				if hostname, err := os.Hostname(); err == nil {
					subFolder = hostname
				}
			}
		}

		name := filepath.Join(ctx.Output, subFolder, path)

		if !info.IsDir() { //nolint:nestif
			const perm = 0o755

			if _, err := os.Stat(filepath.Dir(name)); os.IsNotExist(err) {
				if err := os.MkdirAll(filepath.Dir(name), perm); err != nil {
					return errors.Unwrap(err)
				}
			}

			output := filepath.Join(ctx.Output, subFolder, path)

			if i.Squash {
				output = filepath.Join(ctx.Output, info.Name())
			}

			file, err := i.copy(path, output)
			if err != nil {
				return err
			}
			files = append(files, file)
		}

		return nil
	})
	if err != nil {
		ctx.Log.Error(err)

		return nil, nil
	}

	return files, nil
}

func (i *folder) Output(ctx *core.Context, cfg interface{}, files ...*os.File) error {
	if err := mapstructure.Decode(cfg, &i); err != nil {
		ctx.Log.Error(err)

		return nil
	}

	if err := validator.New().Struct(i); err != nil {
		ctx.Log.Error(err)

		return nil
	}

	for _, file := range files {
		name := filepath.Join(i.Path, strings.TrimPrefix(file.Name(), ctx.BaseOutput()))

		if _, err := os.Stat(filepath.Dir(name)); os.IsNotExist(err) {
			const perm = 0o755

			if err := os.MkdirAll(filepath.Dir(name), perm); err != nil {
				return errors.Unwrap(err)
			}
		}

		newFile, err := i.copy(file.Name(), name)
		if err != nil {
			ctx.Log.Error(err)

			break
		}

		ctx.Log.Infof("Added %s", newFile.Name())
	}

	return nil
}

func (i folder) copy(input, output string) (*os.File, error) {
	file, err := os.Open(input)
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	stat, err := file.Stat()
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	newFile, err := os.OpenFile(output, os.O_CREATE|os.O_RDWR, stat.Mode())
	if err != nil {
		return nil, errors.Unwrap(err)
	}

	if _, err := io.Copy(newFile, file); err != nil {
		return nil, errors.Unwrap(err)
	}

	if err := file.Close(); err != nil {
		return nil, errors.Unwrap(err)
	}

	if err := newFile.Close(); err != nil {
		return nil, errors.Unwrap(err)
	}

	if i.RemoveSource {
		if err := os.Remove(input); err != nil {
			return nil, errors.Unwrap(err)
		}
	}

	return newFile, nil
}
