# [Plugins:InputOutput] Folder

Plugin for backup your files and folders.

## Configuration

| Key           | Type      | Description                                   |          |
|---------------|-----------|-----------------------------------------------|----------|
| name          | string    | Task name                                     | optional |
| path          | string    | Path of ressources                            | required |
| absolute      | bool      | Create absolute structure                     | optional |
| squash        | bool      | Move to root backup path                      | optional |
| removeSource  | bool      | Remove source file                            | optional |

## Example (`settings.yaml`)

```yaml
input:
  folder:
    - name: folder1
      path: /usr/local/share/folder1
      absolute: true

    - name: file2
      path: /root/settings.yaml
      squash: true
      
    - name: folder2
      path: /tmp/folder
      removeSource: true

output:
  folder:
    - name: nfs
      path: /mnt/nfs/backup
```
