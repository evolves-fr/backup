# [Plugins:Output] S3

Plugin for send backup to S3.

## Configuration

| Key       | Type      | Description                                   |          |
|-----------|-----------|-----------------------------------------------|----------|
| name      | string    | Task name                                     | optional |
| endpoint  | string    | S3 endpoint                                   | required |
| accessID  | string    | S3 accessID                                   | required |
| secretID  | string    | S3 secretID                                   | required |
| bucket    | string    | S3 bucket name                                | required |
| region    | string    | S3 region                                     | optional |
| location  | string    | S3 bucket location                            | optional |
| ssl       | bool      | S3 over SSL                                   | optional |
| squash    | bool      | Move to root backup path                      | optional |

## Example (`settings.yaml`)

```yaml
output:
  s3:
    - name: localMinio
      endpoint: 127.0.0.1:9000
      accessID: minioadmin
      secretID: minioadmin
      bucket: backup

  - name: scaleway
      endpoint: s3.fr-par.scw.cloud
      region: fr-par
      accessID: myAccessScalwayID
      secretID: mySecretScalwayID
      bucket: myBucket
```
