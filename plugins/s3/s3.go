package s3

import (
	"context"
	"errors"
	"io"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/evolves-fr/backup/core"
)

func New() core.OutputPlugin {
	return &s3{}
}

type s3 struct {
	Endpoint    string         `json:"endpoint" yaml:"endpoint" mapstructure:"endpoint" validate:"required"`
	AccessKeyID string         `json:"accessID" yaml:"accessID" mapstructure:"accessID" validate:"required"`
	SecretKeyID string         `json:"secretID" yaml:"secretID" mapstructure:"secretID" validate:"required"`
	Location    string         `json:"location" yaml:"location" mapstructure:"location" validate:""`
	Bucket      string         `json:"bucket" yaml:"bucket" mapstructure:"bucket" validate:"required"`
	Region      string         `json:"region" yaml:"region" mapstructure:"region" validate:""`
	Prefix      string         `json:"prefix" yaml:"prefix" mapstructure:"prefix" validate:""`
	SSL         bool           `json:"ssl" yaml:"ssl" mapstructure:"ssl" validate:""`
	Squash      bool           `json:"squash" yaml:"squash" mapstructure:"squash"`
	Lifetime    *time.Duration `json:"lifetime,omitempty" yaml:"lifetime,omitempty" mapstructure:"lifetime"`

	client *minio.Client
}

func (*s3) Key() string {
	return "s3"
}

func (*s3) Name() string {
	return "S3"
}

func (*s3) Description() string {
	return "S3 plugin"
}

func (i *s3) Output(ctx *core.Context, cfg interface{}, files ...*os.File) error {
	decoderConfig := &mapstructure.DecoderConfig{
		DecodeHook: mapstructure.ComposeDecodeHookFunc(
			mapstructure.StringToTimeDurationHookFunc(),
		),
		Metadata: nil,
		Result:   &i,
	}

	decoder, err := mapstructure.NewDecoder(decoderConfig)
	if err != nil {
		return errors.Unwrap(err)
	}

	if err := decoder.Decode(cfg); err != nil {
		return errors.Unwrap(err)
	}

	if err := validator.New().Struct(i); err != nil {
		return errors.Unwrap(err)
	}

	options := &minio.Options{
		Creds:  credentials.NewStaticV4(i.AccessKeyID, i.SecretKeyID, ""),
		Region: i.Region,
		Secure: i.SSL,
	}

	i.client, err = minio.New(i.Endpoint, options)
	if err != nil {
		return errors.Unwrap(err)
	}

	hasBucket, err := i.client.BucketExists(context.Background(), i.Bucket)
	if err != nil {
		return errors.Unwrap(err)
	}

	if !hasBucket {
		makeBucketOptions := minio.MakeBucketOptions{
			Region: i.Location,
		}

		if err := i.client.MakeBucket(context.Background(), i.Bucket, makeBucketOptions); err != nil {
			return errors.Unwrap(err)
		}
	}

	for _, file := range files {
		if err := i.putObject(ctx, file); err != nil {
			return err
		}
	}

	if i.Lifetime != nil {
		ctx.Log.Infof("Check backup retention (%s)", i.Lifetime.String())

		if err := i.checkAndRemoveOlderBackup(ctx); err != nil {
			return err
		}
	}

	return nil
}

func (i *s3) checkAndRemoveOlderBackup(ctx *core.Context) error {
	objects, err := i.listObjects("", false)
	if err != nil {
		return err
	}

	for _, object := range objects {
		if object.IsExpired(*i.Lifetime) {
			var err error

			ctx.Log.Infof("Remove %s", object.Key)

			if object.Size > 0 {
				err = i.removeObject(object)
			} else {
				err = i.removeDirectory(ctx, object)
			}

			if err != nil {
				ctx.Log.Error(err)

				continue
			}
		}
	}

	return nil
}

func (i *s3) listObjects(prefix string, recursive bool) ([]Object, error) {
	objects := make([]Object, 0)

	ctxCancel, cancel := context.WithCancel(context.Background())
	defer cancel()

	objectCh := i.client.ListObjects(ctxCancel, i.Bucket, minio.ListObjectsOptions{
		Prefix:    path.Join(i.Prefix, prefix) + "/",
		Recursive: recursive,
	})
	for object := range objectCh {
		if object.Err != nil {
			return nil, object.Err
		}

		objects = append(objects, Object(object))
	}

	return objects, nil
}

func (i *s3) putObject(ctx *core.Context, file *os.File) error {
	var err error

	file, err = os.Open(file.Name())
	if err != nil {
		return errors.Unwrap(err)
	}

	const bufferSize = 512
	buf := make([]byte, bufferSize)
	bytesRead, err := file.Read(buf)

	if err != nil && !errors.Is(err, io.EOF) {
		return errors.Unwrap(err)
	}

	putObjectOptions := minio.PutObjectOptions{ContentType: http.DetectContentType(buf[:bytesRead])}

	name := strings.TrimPrefix(strings.TrimPrefix(file.Name(), filepath.Clean(ctx.BaseOutput())), "/")

	if i.Squash {
		name = strings.TrimPrefix(strings.TrimPrefix(file.Name(), filepath.Clean(ctx.Output)), "/")
	}

	ctxCancel, cancel := context.WithCancel(context.Background())
	defer cancel()

	name = path.Join(i.Prefix, name)

	if _, err = i.client.FPutObject(ctxCancel, i.Bucket, name, file.Name(), putObjectOptions); err != nil {
		return errors.Unwrap(err)
	}

	ctx.Log.Infof("Sent %s", file.Name())

	return errors.Unwrap(file.Close())
}

func (i *s3) removeObject(object Object) error {
	ctxCancel, cancel := context.WithCancel(context.Background())
	defer cancel()

	return errors.Unwrap(i.client.RemoveObject(ctxCancel, i.Bucket, object.Key, minio.RemoveObjectOptions{}))
}

func (i *s3) removeDirectory(ctx *core.Context, directory Object) error {
	objects, err := i.listObjects(directory.Key, true)
	if err != nil {
		return err
	}

	var wg sync.WaitGroup
	for _, object := range objects {
		wg.Add(1)

		go func(object Object) {
			defer wg.Done()

			if err := i.removeObject(object); err != nil {
				ctx.Log.Errorf("(%s) %v", object.Key, err)
			}
		}(object)
	}

	wg.Wait()

	return i.removeObject(directory)
}

type Object minio.ObjectInfo

func (o Object) IsExpired(exp time.Duration) bool {
	name := path.Base(o.Key)
	for strings.Contains(name, ".") {
		name = strings.TrimSuffix(name, path.Ext(name))
	}

	createdAt, err := time.Parse("20060102T150405", name)
	if err != nil {
		return false
	}

	return time.Now().UTC().Add(-exp).After(createdAt)
}
