package archive

import (
	"errors"
	"os"
	"path/filepath"

	"github.com/mitchellh/mapstructure"
	"gitlab.com/evolves-fr/backup/core"
	"gitlab.com/evolves-fr/backup/pkg/archive"
	"gitlab.com/evolves-fr/backup/pkg/sum"
)

func New() core.MiddlewarePlugin {
	return &plugin{}
}

type plugin struct {
	Filename string `json:"filename" yaml:"filename" mapstructure:"filename"`
	Checksum bool   `json:"checksum" yaml:"checksum" mapstructure:"checksum"`
}

func (*plugin) Key() string {
	return "archive"
}

func (*plugin) Name() string {
	return "Archive"
}

func (*plugin) Description() string {
	return "Create an archive from inputs"
}

func (i *plugin) Middleware(ctx *core.Context, cfg interface{}, files []*os.File) ([]*os.File, error) {
	if err := mapstructure.Decode(cfg, &i); err != nil {
		return nil, errors.Unwrap(err)
	}

	if i.Filename == "" {
		i.Filename = filepath.Base(ctx.Output)
	}

	f, err := archive.NewTarGz(i.Filename+".tar.gz", ctx.BaseOutput(), files...)
	if err != nil {
		return nil, errors.Unwrap(err)
	}
	defer f.Close()

	files = []*os.File{f}
	ctx.Log.Infof("Added %s", f.Name())

	if i.Checksum {
		fc, err := sum.New(i.Filename+".sha256", ctx.BaseOutput(), files...)
		if err != nil {
			return nil, errors.Unwrap(err)
		}

		files = append(files, fc)
		ctx.Log.Infof("Added %s", fc.Name())
	}

	return files, nil
}
