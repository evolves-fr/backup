# [Plugin::Middleware] Archive

Create an archive from inputs.

## Configuration

| Key       | Type      | Description                                   |          |
|-----------|-----------|-----------------------------------------------|----------|
| name      | string    | Task name                                     | optional |
| filename  | string    | Filename of archive (without extension)       | optional |
| checksum  | bool      | Create a checksum for archive                 | optional |

## Example (`settings.yaml`)
### Default
```yaml
middlewares:
  archive: {}
```

### Custom
```yaml
middlewares:
  archive:
    filename: myCustomArchive
    checksum: true
```
