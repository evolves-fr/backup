# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.6.1] - 2021-10-04
### Fixed
- CVE-2020-29652

## [v0.6.0] - 2021-10-04
### Added
- (plugins/folder): Remove source file

## [v0.5.1] - 2021-10-04
### Fixed
- (plugins/firebase/storage): Uncomment reader

## [v0.5.0] - 2021-10-04
### Changed
- (docker) Bump alpine from 3.13.3 to 3.14.2

## [v0.4.1] - 2021-10-03
### Fixed
- (plugins/firebase/storage): Set file if diff disabled

## [v0.4.0] - 2021-10-03
### Added
- (core) DecodeAndValidate in context
- (core) Disable sub-folder timestamp
- (plugins) Firebase:Firestore
- (plugins) Firebase:Storage

### Changed
- (core) log

## [v0.3.0] - 2021-10-01
### Fixed
- (core) tests

### Changed
- (core) lint
- (main) lint
- (pkg) lint
- (plugins) lint

### Added
- (plugins/s3) Prefix option

## [v0.2.0] - 2021-10-01
### Fixed
- (core) Middleware error log
- (pkg/archive) File closer
- (pkg/archive) Output buffer (for not use memory)

### Added
- (plugins/s3) Add backup lifetime

## [v0.1.0] - 2021-03-26
### Added
- Initial release

[Unreleased]: https://gitlab.com/evolves-fr/backup/compare/v0.6.1...master
[v0.6.1]: https://gitlab.com/evolves-fr/backup/compare/v0.6.0...v0.6.1
[v0.6.0]: https://gitlab.com/evolves-fr/backup/compare/v0.5.1...v0.6.0
[v0.5.1]: https://gitlab.com/evolves-fr/backup/compare/v0.5.0...v0.5.1
[v0.5.0]: https://gitlab.com/evolves-fr/backup/compare/v0.4.1...v0.5.0
[v0.4.1]: https://gitlab.com/evolves-fr/backup/compare/v0.4.0...v0.4.1
[v0.4.0]: https://gitlab.com/evolves-fr/backup/compare/v0.3.0...v0.4.0
[v0.3.0]: https://gitlab.com/evolves-fr/backup/compare/v0.2.0...v0.3.0
[v0.2.0]: https://gitlab.com/evolves-fr/backup/compare/v0.1.0...v0.2.0
[v0.1.0]: https://gitlab.com/evolves-fr/backup/tags/v0.1.0
