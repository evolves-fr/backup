<div align="center">

[![backup](https://gitlab.com/evolves-fr/backup/raw/master/docs/backup-dark.png)](https://gitlab.com/evolves-fr/backup)

# Backup

[![Go Report Card](https://goreportcard.com/badge/gitlab.com/evolves-fr/backup?style=for-the-badge)](https://goreportcard.com/report/gitlab.com/evolves-fr/backup)
[![Maturity](https://img.shields.io/badge/Maturity-Alpha-red?style=for-the-badge)]()
[![License](https://img.shields.io/badge/license-MIT-brightgreen?style=for-the-badge)](https://gitlab.com/evolves-fr/backup/blob/master/LICENSE.md)

[![ChartVersion](https://img.shields.io/badge/dynamic/yaml?style=for-the-badge&label=Chart%20Version&query=entries.backup[0].version&url=http%3A%2F%2Fcharts.evolves.fr%2F)](https://artifacthub.io/packages/helm/evolves/backup)
[![AppVersion](https://img.shields.io/badge/dynamic/yaml?style=for-the-badge&label=Chart%20App%20Version&query=entries.backup[0].appVersion&url=http%3A%2F%2Fcharts.evolves.fr%2F)](https://gitlab.com/evolves-fr/backup/-/releases)
[![Artifact HUB](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/evolves&style=for-the-badge)](https://artifacthub.io/packages/search?repo=evolves)

</div><br>

*Backup* is an open-source tools written in Go (Golang). Create backup from multi sources, to multi destinations.


## Plugins

| Name                            | Description                                   | Input     | Middleware | Output    |
|---------------------------------|-----------------------------------------------|:---------:|:----------:|:---------:|
| [mysql](./plugins/mysql)        | Backup [MySQL] database with mysqldump binary | X         |            |           |
| [folder](./plugins/folder)      | Copy files and folder                         | X         |            | X         |
| [s3](./plugins/s3)              | Send files to [Amazon S3]                     |           |            | x         |
| [archive](./plugins/archive)    | Archive into .tar.gz inputs                   |           | X          |           |
| [checksum](./plugins/checksum)  | Create a file with inputs checksum            |           | X          |           |

## Credit

- [urfave/cli](https://github.com/urfave/cli) - for command line
- [jinzhu/configor](https://github.com/jinzhu/configor) - for configuration
- [goreleaser/goreleaser](https://github.com/goreleaser/goreleaser) - for release
- [jamf/go-mysqldump](github.com/jamf/go-mysqldump) - for dump MySQL databases
- [minio/minio-go](https://github.com/minio/minio-go) - for S3 like

## Icon

From [iconduck](https://iconduck.com/icons/59460/backup)

[Amazon S3]: https://aws.amazon.com/s3/
[MySQL]: https://www.mysql.com/
