package core

import "errors"

var ContextCantNil = errors.New("context can't nil")
