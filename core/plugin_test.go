package core

import "os"

/*
 * Input
 */
type input struct{}

func (*input) Key() string {
	return "test"
}

func (*input) Name() string {
	return "Test"
}

func (*input) Description() string {
	return "Test"
}

func (*input) Input(ctx *Context, cfg interface{}) ([]*os.File, error) {
	return nil, nil
}

/*
 * Output
 */
type output struct{}

func (*output) Key() string {
	return "test"
}

func (*output) Name() string {
	return "Test"
}

func (*output) Description() string {
	return "Test"
}

func (*output) Output(ctx *Context, cfg interface{}, files ...*os.File) error {
	return nil
}

/*
 * Middleware
 */
type middleware struct{}

func (*middleware) Key() string {
	return "test"
}

func (*middleware) Name() string {
	return "Test"
}

func (*middleware) Description() string {
	return "Test"
}

func (*middleware) Middleware(ctx *Context, cfg interface{}, files []*os.File) ([]*os.File, error) {
	return nil, nil
}

/*
 * Plugin
 */
type base struct{}

func (*base) Key() string {
	return "test"
}

func (*base) Name() string {
	return "Test"
}

func (*base) Description() string {
	return "Test"
}
