package core

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestContext_BaseOutput(t *testing.T) {
	ctx := &Context{
		Debug:  false,
		Output: "tmp/test",
		Log:    nil,
	}

	assert.Equal(t, "tmp", ctx.BaseOutput())
}

func TestContext_DecodeAndValidate(t *testing.T) {
	ctx := &Context{}

	type Test struct {
		Key1 string `mapstructure:"key1" validate:"required"`
		Key2 string `mapstructure:"key2"`
	}

	res1 := Test{}
	err1 := ctx.DecodeAndValidate(map[string]string{"key1": "value1", "key2": "value2"}, &res1)
	assert.NoError(t, err1)
	assert.EqualValues(t, Test{Key1: "value1", Key2: "value2"}, res1)

	res2 := Test{}
	err2 := ctx.DecodeAndValidate(map[string]string{"key2": "value2"}, &res2)
	assert.Error(t, err2)
	assert.EqualValues(t, Test{Key2: "value2"}, res2)

	err3 := ctx.DecodeAndValidate("not valid input", map[string]string{})
	assert.Error(t, err3)
}
