package core

import (
	"os"
	"testing"

	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
)

func TestCoreContext_plugin(t *testing.T) {
	l := core{
		log:        logrus.New(),
		plugins:    make(map[string]plugin),
		files:      make([]*os.File, 0),
		debug:      false,
		backupPath: "tmp",
		outputPath: "tmp/test",
	}

	c1 := l.context(&input{}, nil)
	assert.Equal(t, false, c1.Debug)
	assert.Equal(t, "tmp", c1.BaseOutput())
	assert.Equal(t, "tmp/test", c1.Output)
	assert.Equal(t, logrus.Fields{"plugin": "input::test"}, c1.Log.Data)

	c2 := l.context(&output{}, nil)
	assert.Equal(t, false, c2.Debug)
	assert.Equal(t, "tmp", c2.BaseOutput())
	assert.Equal(t, "tmp/test", c2.Output)
	assert.Equal(t, logrus.Fields{"plugin": "output::test"}, c2.Log.Data)

	c3 := l.context(&middleware{}, nil)
	assert.Equal(t, false, c3.Debug)
	assert.Equal(t, "tmp", c3.BaseOutput())
	assert.Equal(t, "tmp/test", c3.Output)
	assert.Equal(t, logrus.Fields{"plugin": "middleware::test"}, c3.Log.Data)

	c4 := l.context(&base{}, nil)
	assert.Equal(t, false, c4.Debug)
	assert.Equal(t, "tmp", c4.BaseOutput())
	assert.Equal(t, "tmp/test", c4.Output)
	assert.Equal(t, logrus.Fields{"plugin": "unknown::test"}, c4.Log.Data)
}

func TestCoreContext_configString(t *testing.T) {
	l := core{
		log:        logrus.New(),
		plugins:    make(map[string]plugin),
		files:      make([]*os.File, 0),
		debug:      false,
		backupPath: "tmp",
		outputPath: "tmp/test",
	}

	c5 := l.context(&input{}, "test")
	assert.Equal(t, false, c5.Debug)
	assert.Equal(t, "tmp", c5.BaseOutput())
	assert.Equal(t, "tmp/test", c5.Output)
	assert.Equal(t, logrus.Fields{"plugin": "input::test", "config": "test"}, c5.Log.Data)

	c6 := l.context(&output{}, "test")
	assert.Equal(t, false, c6.Debug)
	assert.Equal(t, "tmp", c6.BaseOutput())
	assert.Equal(t, "tmp/test", c6.Output)
	assert.Equal(t, logrus.Fields{"plugin": "output::test", "config": "test"}, c6.Log.Data)

	c7 := l.context(&middleware{}, "test")
	assert.Equal(t, false, c7.Debug)
	assert.Equal(t, "tmp", c7.BaseOutput())
	assert.Equal(t, "tmp/test", c7.Output)
	assert.Equal(t, logrus.Fields{"plugin": "middleware::test", "config": "test"}, c7.Log.Data)

	c8 := l.context(&base{}, "test")
	assert.Equal(t, false, c8.Debug)
	assert.Equal(t, "tmp", c8.BaseOutput())
	assert.Equal(t, "tmp/test", c8.Output)
	assert.Equal(t, logrus.Fields{"plugin": "unknown::test", "config": "test"}, c8.Log.Data)
}

func TestCoreContext_configMap(t *testing.T) {
	l := core{
		log:        logrus.New(),
		plugins:    make(map[string]plugin),
		files:      make([]*os.File, 0),
		debug:      false,
		backupPath: "tmp",
		outputPath: "tmp/test",
	}

	c9 := l.context(&input{}, map[interface{}]interface{}{"name": "test"})
	assert.Equal(t, false, c9.Debug)
	assert.Equal(t, "tmp", c9.BaseOutput())
	assert.Equal(t, "tmp/test", c9.Output)
	assert.Equal(t, logrus.Fields{"plugin": "input::test", "config": "test"}, c9.Log.Data)

	c10 := l.context(&output{}, map[interface{}]interface{}{"name": "test"})
	assert.Equal(t, false, c10.Debug)
	assert.Equal(t, "tmp", c10.BaseOutput())
	assert.Equal(t, "tmp/test", c10.Output)
	assert.Equal(t, logrus.Fields{"plugin": "output::test", "config": "test"}, c10.Log.Data)

	c11 := l.context(&middleware{}, map[interface{}]interface{}{"name": "test"})
	assert.Equal(t, false, c11.Debug)
	assert.Equal(t, "tmp", c11.BaseOutput())
	assert.Equal(t, "tmp/test", c11.Output)
	assert.Equal(t, logrus.Fields{"plugin": "middleware::test", "config": "test"}, c11.Log.Data)

	c12 := l.context(&base{}, map[interface{}]interface{}{"name": "test"})
	assert.Equal(t, false, c12.Debug)
	assert.Equal(t, "tmp", c12.BaseOutput())
	assert.Equal(t, "tmp/test", c12.Output)
	assert.Equal(t, logrus.Fields{"plugin": "unknown::test", "config": "test"}, c12.Log.Data)
}
