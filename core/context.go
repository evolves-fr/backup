package core

import (
	"path/filepath"

	"github.com/go-playground/validator/v10"
	"github.com/mitchellh/mapstructure"
	"github.com/sirupsen/logrus"
)

type Context struct {
	Debug  bool
	Output string
	Log    *logrus.Entry
}

func (c Context) BaseOutput() string {
	return filepath.Dir(c.Output)
}

func (c Context) DecodeAndValidate(input, output interface{}) error {
	if err := mapstructure.Decode(input, &output); err != nil {
		return err
	}

	return validator.New().Struct(output)
}
