package core

import (
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"time"

	formatter "github.com/antonfisher/nested-logrus-formatter"
	"github.com/sirupsen/logrus"
)

type Core interface {
	Logger() *logrus.Logger
	Register(plugin plugin)
	Run(settings *Settings)
}

func New(tempPath, logPath string, debug bool) Core {
	log := logrus.New()
	log.SetOutput(os.Stdout)
	log.SetLevel(logrus.InfoLevel)
	log.SetFormatter(&formatter.Formatter{
		FieldsOrder:           []string{"plugin", "config"},
		TimestampFormat:       time.RFC3339,
		HideKeys:              true,
		NoColors:              false,
		NoFieldsColors:        false,
		NoFieldsSpace:         false,
		ShowFullLevel:         false,
		NoUppercaseLevel:      false,
		TrimMessages:          false,
		CallerFirst:           false,
		CustomCallerFormatter: nil,
	})

	if debug {
		log.SetLevel(logrus.TraceLevel)
	}

	if logPath != "" && logPath != "/dev/stdout" {
		const perm = 0o744

		if f, err := os.OpenFile(logPath, os.O_CREATE|os.O_APPEND|os.O_RDWR, perm); err == nil {
			log.SetOutput(f)
		}
	}

	return &core{
		files:      make([]*os.File, 0),
		backupPath: tempPath,
		outputPath: tempPath,
		log:        log,
		plugins:    make(map[string]plugin),
		debug:      debug,
		hasErrors:  false,
	}
}

type core struct {
	files      []*os.File
	backupPath string
	outputPath string
	log        *logrus.Logger
	plugins    map[string]plugin
	debug      bool
	hasErrors  bool
}

func (l *core) Logger() *logrus.Logger {
	return l.log
}

func (l *core) Register(p plugin) {
	if _, ok := l.plugins[p.Key()]; ok {
		l.log.Warnf("plugin %s already registered", p.Key())
	} else {
		l.plugins[p.Key()] = p
		switch p.(type) {
		case InputOutputPlugin:
			l.log.WithField("plugin", "input::"+p.Key()).Debug("registered")
			l.log.WithField("plugin", "output::"+p.Key()).Debug("registered")
		case InputPlugin:
			l.log.WithField("plugin", "input::"+p.Key()).Debug("registered")
		case OutputPlugin:
			l.log.WithField("plugin", "output::"+p.Key()).Debug("registered")
		case MiddlewarePlugin:
			l.log.WithField("plugin", "middleware::"+p.Key()).Debug("registered")
		}
	}
}

func (l *core) Run(settings *Settings) {
	if _, err := os.Stat(l.outputPath); !os.IsExist(err) {
		const perm = 0o755

		if err := os.MkdirAll(l.outputPath, perm); err != nil {
			l.log.Fatal(err)
		}
	}

	if !settings.NoSubFolder {
		l.outputPath = filepath.Join(l.outputPath, time.Now().UTC().Format("20060102T150405"))
	}

	l.inputs(settings.Inputs)
	l.middlewares(settings.Middlewares)
	l.outputs(settings.Outputs)

	if !settings.DisableClean {
		if err := os.RemoveAll(l.backupPath); err != nil {
			l.log.Fatal(err)
		}
	}

	if l.hasErrors {
		os.Exit(1)
	}
}

func (l *core) context(p plugin, c interface{}) *Context {
	var pType string

	switch p.(type) {
	case InputPlugin:
		pType = "input"
	case OutputPlugin:
		pType = "output"
	case MiddlewarePlugin:
		pType = "middleware"
	default:
		pType = "unknown"
	}

	fields := logrus.Fields{
		"plugin": pType + "::" + p.Key(),
	}

	if c != nil {
		switch r := reflect.ValueOf(c); r.Type().Kind() { //nolint:exhaustive
		case reflect.Map:
			if v, ok := c.(map[interface{}]interface{})["name"]; ok {
				fields["config"] = v
			}
		default:
			fields["config"] = fmt.Sprintf("%v", c)
		}
	}

	return &Context{
		Debug:  l.debug,
		Output: l.outputPath,
		Log:    l.log.WithFields(fields),
	}
}

func (l *core) inputs(inputs map[string][]interface{}) {
	for key, configs := range inputs {
		for _, config := range configs {
			p, ok := l.plugins[key].(InputPlugin)
			if !ok {
				l.hasErrors = true
				l.log.WithField("plugin", "input::"+key).Errorf("plugin not registered")

				return
			}

			ctx := l.context(p, config)

			files, err := p.Input(ctx, config)
			if err != nil {
				l.hasErrors = true

				ctx.Log.Error(err)

				return
			}

			for _, file := range files {
				l.files = append(l.files, file)

				ctx.Log.Infof("Added %s", file.Name())
			}
		}
	}
}

func (l *core) middlewares(middlewares map[string]interface{}) {
	for key, config := range middlewares {
		p, ok := l.plugins[key].(MiddlewarePlugin)
		if !ok {
			l.hasErrors = true
			l.log.WithField("plugin", "middleware::"+key).Errorf("plugin not registered")

			return
		}

		ctx := l.context(p, config)

		files, err := p.Middleware(ctx, config, l.files)
		if err != nil {
			l.hasErrors = true

			ctx.Log.Error(err)

			return
		}

		l.files = files
	}
}

func (l *core) outputs(outputs map[string][]interface{}) {
	for key, configs := range outputs {
		for _, config := range configs {
			p, ok := l.plugins[key].(OutputPlugin)
			if !ok {
				l.hasErrors = true

				l.log.WithField("plugin", "output::"+key).Errorf("plugin not registered")

				return
			}

			ctx := l.context(p, config)

			if err := p.Output(ctx, config, l.files...); err != nil {
				l.hasErrors = true

				ctx.Log.Error(err)
			}
		}
	}
}
