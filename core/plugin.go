package core

import "os"

type (
	plugin interface {
		Key() string
		Name() string
		Description() string
	}

	InputPlugin interface {
		plugin
		Input(ctx *Context, cfg interface{}) ([]*os.File, error)
	}

	OutputPlugin interface {
		plugin
		Output(ctx *Context, cfg interface{}, files ...*os.File) error
	}

	InputOutputPlugin interface {
		InputPlugin
		OutputPlugin
	}

	MiddlewarePlugin interface {
		plugin
		Middleware(ctx *Context, cfg interface{}, files []*os.File) ([]*os.File, error)
	}
)
