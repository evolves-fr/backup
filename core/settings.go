package core

type Settings struct {
	Inputs       map[string][]interface{} `json:"inputs" yaml:"inputs"`
	Middlewares  map[string]interface{}   `json:"middlewares" yaml:"middlewares"`
	Outputs      map[string][]interface{} `json:"outputs" yaml:"outputs"`
	DisableClean bool                     `json:"disableClean" yaml:"disableClean"`
	NoSubFolder  bool                     `json:"noSubFolder" yaml:"noSubFolder"`
}
