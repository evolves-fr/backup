FROM alpine:3.14.2

LABEL "org.opencontainers.image.title"="backup"
LABEL "org.opencontainers.image.created"="unknown"
LABEL "org.opencontainers.image.revision"="none"
LABEL "org.opencontainers.image.version"="dev"

# Copy binary
COPY backup /usr/local/bin/backup

# Default command is backup
CMD backup
